package com.bugtsa.photogallery.utils;

public interface ConstantManager {
    String PREF_SEARCH_QUERY = "searchQuery";

    String PREF_LAST_RESULT_ID = "lastResultId";

    String PREF_IS_ALARM_ON = "isAlarmOn";

    int RESPONSE_OK = 200;
    int USER_NOT_AUTHORIZED = 401;
    int LOGIN_OR_PASSWORD_INCORRECT = 404;
    int RESPONSE_NOT_OK = 666;
    int SERVER_ERROR = 677;
}
