package com.bugtsa.photogallery.data.managers;

import android.content.SharedPreferences;

import com.bugtsa.photogallery.PhotoGalleryApplication;
import com.bugtsa.photogallery.utils.ConstantManager;

public class PreferencesManager {
    private static SharedPreferences mSharedPreferences;

    public PreferencesManager() {
        mSharedPreferences = PhotoGalleryApplication.getSharedPreferences();
    }

    public static String getStoredQuery() {
        return mSharedPreferences
                .getString(ConstantManager.PREF_SEARCH_QUERY, null);
    }

    public static void setStoredQuery(String query) {
        mSharedPreferences
                .edit()
                .putString(ConstantManager.PREF_SEARCH_QUERY, query)
                .apply();
    }

    public static String getLastResultId() {
        return mSharedPreferences
                .getString(ConstantManager.PREF_LAST_RESULT_ID, null);
    }

    public static void setLastResultId(String lastResultId) {
        mSharedPreferences
                .edit()
                .putString(ConstantManager.PREF_LAST_RESULT_ID, lastResultId)
                .apply();
    }

    public static boolean isAlarmOn() {
        return mSharedPreferences
                .getBoolean(ConstantManager.PREF_IS_ALARM_ON, false);
    }

    public static void setAlarmOn(boolean isOn) {
        mSharedPreferences
                .edit()
                .putBoolean(ConstantManager.PREF_IS_ALARM_ON, isOn)
                .apply();
    }
}
