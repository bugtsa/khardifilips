package com.bugtsa.photogallery.ui.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bugtsa.photogallery.R;
import com.bugtsa.photogallery.data.events.GetPhotoListEvent;
import com.bugtsa.photogallery.data.managers.DataManager;
import com.bugtsa.photogallery.data.managers.PreferencesManager;
import com.bugtsa.photogallery.data.models.Photo;
import com.bugtsa.photogallery.data.network.FlickrFetchr;
import com.bugtsa.photogallery.data.network.PollService;
import com.bugtsa.photogallery.data.network.ThumbnailDownloader;
import com.bugtsa.photogallery.ui.activities.PhotoPageActivity;
import com.bugtsa.photogallery.ui.adapters.PhotoAdapter;
import com.bugtsa.photogallery.ui.adapters.interfaces.CustomClickListener;
import com.bugtsa.photogallery.ui.adapters.interfaces.OnLoadMoreListener;
import com.bugtsa.photogallery.ui.fragments.abstracts.VisibleFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoGalleryFragment extends VisibleFragment {

    private static final String TAG = PhotoGalleryFragment.class.getSimpleName();

    private int perPage = 2;

    private static int pageNumber;

    private List<Photo> mPhotos;

    private ThumbnailDownloader<PhotoAdapter.PhotoHolder> mThumbnailDownloader;

    private DataManager mDataManager;

    private PhotoAdapter mPhotoAdapter;

//    private FlickrFetchr mFlickrFetchr;
private HashMap<Integer, String> mRequestPhotoId;

    @BindView(R.id.fragment_photo_gallery_recycler_view)
    RecyclerView mPhotoRecyclerView;

    public static PhotoGalleryFragment newInstance() {

        return new PhotoGalleryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        mRequestPhotoId = new HashMap<>();
        pageNumber = 1;
        mPhotos = new ArrayList<>();
        mDataManager = DataManager.getInstance();
//        mFlickrFetchr = new FlickrFetchr();
        fetchPhotosFromNetwork(pageNumber);


        PollService.setServiceAlarm(getActivity(), true);

        Handler responseHandler = new Handler();
        mThumbnailDownloader = new ThumbnailDownloader<>(responseHandler);
        mThumbnailDownloader.setThumbnailDownloadListener(new ThumbnailDownloader.ThumbnailDownloadListener<PhotoAdapter.PhotoHolder>() {
            @Override
            public void onThumbnailDownloaded(PhotoAdapter.PhotoHolder photoHolder, Bitmap thumbnailBitmap) {
                if (isAdded()) {
                    Drawable drawable = new BitmapDrawable(getResources(), thumbnailBitmap);
                    photoHolder.bindDrawable(drawable);
                }
            }
        });
        mThumbnailDownloader.start();
        mThumbnailDownloader.getLooper();
        Log.i(TAG, "Background thread started");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_gallery, container, false);
        ButterKnife.bind(this, view);

        mPhotoRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        setupAdapter();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_photo_gallery, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "QueryTextChange: " + newText);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "QueryTextSubmit: " + query);
                PreferencesManager.setStoredQuery(query);
                fetchPhotosFromNetwork(pageNumber);
                return true;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String query = PreferencesManager.getStoredQuery();
                searchView.setQuery(query, false);
            }
        });

        MenuItem toggleItem = menu.findItem(R.id.menu_item_toggle_polling);
        if (PollService.isServiceAlarmOn(getActivity())) {
            toggleItem.setTitle(R.string.stop_polling);
        } else {
            toggleItem.setTitle(R.string.start_polling);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_clear:
                PreferencesManager.setStoredQuery(null);
                fetchPhotosFromNetwork(pageNumber);
                return true;
            case R.id.menu_item_toggle_polling:
                boolean shouldStartAlarm =
                        !PollService.isServiceAlarmOn(getActivity());
                PollService.setServiceAlarm(getActivity(), shouldStartAlarm);
                getActivity().invalidateOptionsMenu();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mThumbnailDownloader.quit();
        Log.i(TAG, "Background thread destroyed");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mThumbnailDownloader.clearQueue();
    }

    private void fetchPhotosFromNetwork(int pageNumber) {
        String query = PreferencesManager.getStoredQuery();
        if (query == null) {
            new FlickrFetchr().fetchRecentPhotos(perPage, pageNumber);
        } else {
            new FlickrFetchr().searchPhotos(query);
        }
    }

    @Subscribe
    public void onGetPhotoListEvent(GetPhotoListEvent getPhotoListEvent) {
        List<Photo> tempPhotos = getPhotoListEvent.getPhotoList();
        refreshAdapter(tempPhotos);
    }

    private void refreshAdapter(List<Photo> photos) {
        boolean isAddedPhoto = false;
        for (Photo photo : photos) {
            if (!mRequestPhotoId.containsValue(photo.getId())) {
                mPhotos.add(photo);
                mRequestPhotoId.put(mPhotos.size(), photo.getId());
                mPhotoAdapter.notifyItemInserted(mPhotos.size() - 1);
                isAddedPhoto = true;
            }
        }
        if (!isAddedPhoto) {
            pageNumber++;
            fetchPhotosFromNetwork(pageNumber);
        }
        mPhotoAdapter.setLoaded();
    }

    private void setupAdapter() {
        if (isAdded()) {
            mPhotoAdapter = new PhotoAdapter(mPhotos, mThumbnailDownloader, getContext(), new CustomClickListener() {
                @Override
                public void onPhotoItemClickListener(int position) {
                    Intent intent = PhotoPageActivity.newIntent(getContext(), mPhotos.get(position).getPhotoPageUri());
                    startActivity(intent);
                }
            }, mPhotoRecyclerView);
            mPhotoRecyclerView.setAdapter(mPhotoAdapter);
        }

        mPhotoAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                mPhotos.add(null);
//                mPhotoAdapter.notifyItemInserted(mPhotos.size() - 1);
                ++pageNumber;

                fetchPhotosFromNetwork(pageNumber);
            }
        });
    }
}
