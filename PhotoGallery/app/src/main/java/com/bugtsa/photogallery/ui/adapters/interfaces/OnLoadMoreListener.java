package com.bugtsa.photogallery.ui.adapters.interfaces;

/**
 * Created by Puzya on 09-Sep-16.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
