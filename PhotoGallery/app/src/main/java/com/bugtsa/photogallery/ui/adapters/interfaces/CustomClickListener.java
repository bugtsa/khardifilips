package com.bugtsa.photogallery.ui.adapters.interfaces;

public interface CustomClickListener {
    void onPhotoItemClickListener(int position);
}
