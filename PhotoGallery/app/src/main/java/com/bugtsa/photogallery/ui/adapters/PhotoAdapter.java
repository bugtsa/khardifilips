package com.bugtsa.photogallery.ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bugtsa.photogallery.R;
import com.bugtsa.photogallery.data.models.Photo;
import com.bugtsa.photogallery.data.network.ThumbnailDownloader;
import com.bugtsa.photogallery.ui.adapters.interfaces.CustomClickListener;
import com.bugtsa.photogallery.ui.adapters.interfaces.OnLoadMoreListener;

import java.util.List;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoHolder> {

    private List<Photo> mPhotos;

    private Context mContext;

    private ThumbnailDownloader<PhotoHolder> mThumbnailDownloader;

    private CustomClickListener mCustomClickListener;

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 6;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public PhotoAdapter(List<Photo> photos,
                        ThumbnailDownloader<PhotoHolder> thumbnailDownloader,
                        Context context,
                        CustomClickListener customClickListener, RecyclerView recyclerView) {
        mPhotos = photos;
        mThumbnailDownloader = thumbnailDownloader;
        mContext = context;
        mCustomClickListener = customClickListener;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.gallery_item, parent, false);
        return new PhotoHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotoHolder photoHolder, int position) {
        Photo photo = mPhotos.get(position);
        Drawable placeHolder = mContext.getResources().getDrawable(R.drawable.big_nerd_ranch);
        photoHolder.bindDrawable(placeHolder);
        photoHolder.bindGalleryItem(photo);
        mThumbnailDownloader.queueThumbnail(photoHolder, photo.getUrl());
    }

    @Override
    public int getItemCount() {
        return mPhotos.size();
    }

    public void setLoaded() {
        loading = false;
    }

    public class PhotoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //        @BindView(R.id.fragment_photo_gallery_image_view)
        private ImageView mItemImageView;

        private Photo mPhoto;

        public PhotoHolder(View itemView) {
            super(itemView);
            mItemImageView = (ImageView) itemView.findViewById(R.id.fragment_photo_gallery_image_view);
            itemView.setOnClickListener(this);
        }

        public void bindDrawable(Drawable drawable) {
            mItemImageView.setImageDrawable(drawable);
        }

        public void bindGalleryItem(Photo photo) {
            mPhoto = photo;
        }

        @Override
        public void onClick(View view) {
            if (mCustomClickListener == null) {
                return;
            } else {
                int position = mPhotos.indexOf(mPhotos.get(getAdapterPosition()));
                switch (view.getId()) {
                    case R.id.fragment_photo_gallery_image_view:
                        mCustomClickListener.onPhotoItemClickListener(position);
                        break;
                }
            }
        }
    }
}
