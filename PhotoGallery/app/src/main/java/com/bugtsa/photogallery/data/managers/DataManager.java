package com.bugtsa.photogallery.data.managers;

import android.content.Context;

import com.bugtsa.photogallery.PhotoGalleryApplication;
import com.bugtsa.photogallery.data.network.RestService;
import com.bugtsa.photogallery.data.network.ServiceGenerator;
import com.bugtsa.photogallery.data.network.res.FlickrPhotosListRes;

import retrofit2.Call;

public class DataManager {

    private static DataManager INSTANCE = null;

    private Context mContext;

    private PreferencesManager mPreferencesManager;

    private RestService mRestService;

    public DataManager() {
        mContext = PhotoGalleryApplication.getContext();
        mPreferencesManager = new PreferencesManager();
        mRestService = ServiceGenerator.createService(RestService.class);
    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public Context getContext() {
        return mContext;
    }

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    //--------------Network---------------------

    public Call<FlickrPhotosListRes> getPhotos(int perPage, int pageNumber) {
        return mRestService.getPhotos(perPage, pageNumber);
    }
}
