package com.bugtsa.photogallery.data.events;

import com.bugtsa.photogallery.data.models.Photo;

import java.util.List;

public class GetPhotoListEvent {

    private List<Photo> mPhotoList;

    public GetPhotoListEvent(List<Photo> photoList) {
        mPhotoList = photoList;
    }

    public List<Photo> getPhotoList() {
        return mPhotoList;
    }
}
