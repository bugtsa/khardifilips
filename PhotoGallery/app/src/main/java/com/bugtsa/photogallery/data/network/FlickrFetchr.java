package com.bugtsa.photogallery.data.network;

import android.net.Uri;
import android.util.Log;

import com.bugtsa.photogallery.data.events.GetPhotoListEvent;
import com.bugtsa.photogallery.data.managers.DataManager;
import com.bugtsa.photogallery.data.models.Photo;
import com.bugtsa.photogallery.data.network.res.FlickrPhotosListRes;
import com.bugtsa.photogallery.utils.ConstantManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FlickrFetchr {

    private static final String TAG = FlickrFetchr.class.getSimpleName();

    private static final String API_KEY = "a1d7dd85b9c76367de1d5f5894eabddd";

    private static final String FETCH_RECENT_METHOD = "flickr.photos.getRecent";

    private static final String SEARCH_METHOD = "flickr.photos.search";

    private static final Uri ENDPOINT = Uri
            .parse("https://api.flickr.com/services/rest/")
            .buildUpon()
            .appendQueryParameter("api_key", API_KEY)
            .appendQueryParameter("format", "json")
            .appendQueryParameter("nojsoncallback", "1")
            .appendQueryParameter("extras", "url_s")
            .build();

    private List<Photo> mPhotos;


    public FlickrFetchr() {
        mPhotos = new ArrayList<>();
    }

    public byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() +
                        ": with " +
                        urlSpec);
            }

            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    public void fetchRecentPhotos(int perPage, int pageNumber) {
        Call<FlickrPhotosListRes> call = DataManager.getInstance().getPhotos(perPage, pageNumber);
        call.enqueue(new Callback<FlickrPhotosListRes>() {
            @Override
            public void onResponse(Call<FlickrPhotosListRes> call, Response<FlickrPhotosListRes> response) {
                if (response.code() == ConstantManager.RESPONSE_OK) {
                    Log.d(TAG, "Response ok!");
                    List<FlickrPhotosListRes.FlickrPhoto> photoList = new ArrayList<FlickrPhotosListRes.FlickrPhoto>();
                    for (FlickrPhotosListRes.FlickrPhoto flickrPhoto : response.body().getPhotos().getPhoto()) {
                        photoList.add(flickrPhoto);
                        mPhotos.add(new Photo(flickrPhoto));
                    }
                    EventBus.getDefault().post(new GetPhotoListEvent(mPhotos));
                } else {
                    Log.d(TAG, "Not known response!");
                }
            }

            @Override
            public void onFailure(Call<FlickrPhotosListRes> call, Throwable t) {
                Log.d(TAG, "Failed response!");
            }
        });
    }

    public void searchPhotos(String query) {
        EventBus.getDefault().post(new GetPhotoListEvent(mPhotos));
//        String url = buildUrl(SEARCH_METHOD, query);
//        return downloadGalleryItems(url);
    }

    private String buildUrl(String method, String query) {
        Uri.Builder uriBuilder = ENDPOINT.buildUpon()
                .appendQueryParameter("method", method);
        if (method.equals(SEARCH_METHOD)) {
            uriBuilder.appendQueryParameter("text", query);
        }

        return uriBuilder.build().toString();
    }

    private List<Photo> downloadGalleryItems(String url) {

        List<Photo> items = new ArrayList<>();

        try {
            String jsonString = getUrlString(url);
            Log.i(TAG, "Received JSON:" + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseItems(items, jsonBody);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse JSON", e);
        } catch (IOException ioe) {
            Log.d(TAG, "Failed to fetch items" + ioe);
        }
        return items;
    }

    private void parseItems(List<Photo> items, JSONObject jsonBody)
            throws IOException, JSONException {
        JSONObject photosJsonObject = jsonBody.getJSONObject("photos");
        JSONArray photoJsonArray = photosJsonObject.getJSONArray("photo");

        for (int i = 0; i < photoJsonArray.length(); i++) {
            JSONObject photoJsonObject = photoJsonArray.getJSONObject(i);

            Photo item = new Photo();
            item.setId(photoJsonObject.getString("id"));
            item.setCaption(photoJsonObject.getString("title"));

            if (!photoJsonObject.has("url_s")) {
                continue;
            }

            item.setUrl(photoJsonObject.getString("url_s"));
            item.setOwner(photoJsonObject.getString("owner"));
            items.add(item);
        }
    }
}
