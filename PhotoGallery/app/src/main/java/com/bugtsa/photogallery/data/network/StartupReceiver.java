package com.bugtsa.photogallery.data.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.bugtsa.photogallery.data.managers.PreferencesManager;

public class StartupReceiver extends BroadcastReceiver {

    private static final String TAG = StartupReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Received broadcast intent: " + intent.toString());

        boolean isOn = PreferencesManager.isAlarmOn();
        PollService.setServiceAlarm(context, isOn);
    }
}
