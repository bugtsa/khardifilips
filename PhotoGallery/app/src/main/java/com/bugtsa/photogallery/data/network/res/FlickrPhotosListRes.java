package com.bugtsa.photogallery.data.network.res;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FlickrPhotosListRes {
    @SerializedName("photos")
    public FlickrPhotos photos;

    public FlickrPhotos getPhotos() {
        return photos;
    }

    public class FlickrPhotos {

        @SerializedName("page")
        public int page;

        @SerializedName("pages")
        public String pages;

        @SerializedName("perpage")
        public int perpage;

        @SerializedName("total")
        public String total;

        @SerializedName("photo")
        public ArrayList<FlickrPhoto> photo;

        @SerializedName("stat")
        public String stat;

        public ArrayList<FlickrPhoto> getPhoto() {
            return photo;
        }
    }

    public class FlickrPhoto {

        @SerializedName("id")
        public String id;

        @SerializedName("owner")
        public String owner;

        @SerializedName("secret")
        public String secret;

        @SerializedName("server")
        public String server;

        @SerializedName("farm")
        public int farm;

        @SerializedName("title")
        public String title;

        @SerializedName("ispublic")
        public int ispublic;

        @SerializedName("isfriend")
        public int isfriend;

        @SerializedName("isfamily")
        public int isfamily;

        @SerializedName("url_s")
        public String url_s;

        public int getFarm() {
            return farm;
        }

        public String getId() {
            return id;
        }

        public int getIsfamily() {
            return isfamily;
        }

        public int getIsfriend() {
            return isfriend;
        }

        public int getIspublic() {
            return ispublic;
        }

        public String getOwner() {
            return owner;
        }

        public String getSecret() {
            return secret;
        }

        public String getServer() {
            return server;
        }

        public String getTitle() {
            return title;
        }

        public String getUrl() {
            return url_s;
        }
    }
}
