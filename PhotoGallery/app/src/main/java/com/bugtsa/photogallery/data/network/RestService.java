package com.bugtsa.photogallery.data.network;

import com.bugtsa.photogallery.data.network.res.FlickrPhotosListRes;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RestService {

    @FormUrlEncoded
    @POST("rest/?method=flickr.photos.getRecent")
    Call<FlickrPhotosListRes> getPhotos(@Field("per_page") int perPage, @Field("page") int page);
}
