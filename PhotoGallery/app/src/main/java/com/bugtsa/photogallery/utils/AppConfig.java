package com.bugtsa.photogallery.utils;

public interface AppConfig {
    String BASE_URL = "https://api.flickr.com/services/";
    String API_KEY = "a1d7dd85b9c76367de1d5f5894eabddd";
    int MAX_CONNECT_TIMEOUT = 5000;
    int MAX_READ_TIMEOUT = 5000;
}
