package com.bugtsa.geoquiz;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheatActivity extends AppCompatActivity {

    @BindView(R.id.show_answer_button)
    Button mShowAnswerButton;

    @BindView(R.id.answer_text_view)
    TextView mAnswerTextView;

    @BindView(R.id.work_app_version_api)
    TextView mAppVersionAPI;

    public static final String EXTRA_ANSWER_IS_TRUE =
            "com.bugtsa.geoquiz.answer_is_true";

    public static final String EXTRA_ANSWER_SHOWN =
            "com.bugtsa.geoquiz.answer_shown";

    public static final String KEY_IS_ANSWER_SHOW = "KEY_IS_ANSWER_SHOW";
    public static final String KEY_ANSWER_IS_TRUE = "KEY_ANSWER_IS_TRUE";

    private boolean mAnswerIsTrue;

    private boolean mIsAnswerShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            mIsAnswerShown = savedInstanceState.getBoolean(KEY_IS_ANSWER_SHOW, false);
            mAnswerIsTrue = savedInstanceState.getBoolean(KEY_ANSWER_IS_TRUE, false);
            showAnswer();
        } else {
            mIsAnswerShown = false;
            mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);
        }
        mAppVersionAPI.setText(getString(R.string.api_level) + Build.VERSION.SDK_INT);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean(KEY_IS_ANSWER_SHOW, mIsAnswerShown);
        savedInstanceState.putBoolean(KEY_ANSWER_IS_TRUE, mAnswerIsTrue);
    }

    @OnClick(R.id.show_answer_button)
    protected void onShowAnswer() {
        showAnswer();
    }

    private void showAnswer() {
        if (mAnswerIsTrue) {
            mAnswerTextView.setText(R.string.true_btn);
        } else {
            mAnswerTextView.setText(R.string.false_btn);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int cx = mShowAnswerButton.getWidth() / 2;
            int cy = mShowAnswerButton.getHeight() / 2;
            float radius = mShowAnswerButton.getWidth();
            if (!mIsAnswerShown) {
                Animator anim = ViewAnimationUtils
                        .createCircularReveal(mShowAnswerButton, cx, cy, radius, 0);
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mAnswerTextView.setVisibility(View.VISIBLE);
                        mShowAnswerButton.setVisibility(View.INVISIBLE);
                    }
                });
                anim.start();
            } else {
                mShowAnswerButton.setVisibility(View.INVISIBLE);
            }
        } else {
            mAnswerTextView.setVisibility(View.VISIBLE);
        }

        mIsAnswerShown = true;
        setAnswerShownResult(mIsAnswerShown);
    }

    public static Intent newIntent(Context packageContext, boolean
            answerIsTrue) {
        Intent i = new Intent(packageContext, CheatActivity.class);
        i.putExtra(EXTRA_ANSWER_IS_TRUE, answerIsTrue);
        return i;
    }

    public static boolean wasAnswerShown(Intent result) {
        return result.getBooleanExtra(EXTRA_ANSWER_SHOWN, false);
    }

    private void setAnswerShownResult(boolean isAnswerShown) {
        Intent data = new Intent();
        data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
        setResult(RESULT_OK, data);
    }
}
